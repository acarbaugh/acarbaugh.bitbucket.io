/*
 @licstart  The following is the entire license notice for the JavaScript code in this file.

 The MIT License (MIT)

 Copyright (C) 1997-2020 by Dimitri van Heesch

 Permission is hereby granted, free of charge, to any person obtaining a copy of this software
 and associated documentation files (the "Software"), to deal in the Software without restriction,
 including without limitation the rights to use, copy, modify, merge, publish, distribute,
 sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

 The above copyright notice and this permission notice shall be included in all copies or
 substantial portions of the Software.

 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING
 BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM,
 DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

 @licend  The above is the entire license notice for the JavaScript code in this file
*/
var NAVTREE =
[
  [ "Ayden Carbaugh's Portfolio", "index.html", [
    [ "Introduction", "index.html#sec_intro", null ],
    [ "Homework 0x00", "page_Hx00.html", null ],
    [ "Homework 0x01", "page_Hx01.html", null ],
    [ "Homework 2 and 3", "page_Hx023.html", [
      [ "Homework 0x02", "page_Hx023.html#sec_HW02", null ],
      [ "Homework 0x03", "page_Hx023.html#sec_HW03", null ]
    ] ],
    [ "Lab 0x00", "page_0x00.html", [
      [ "Fibonacci", "page_0x00.html#sec_fib", null ],
      [ "Source Code Link", "page_0x00.html#sec_flink", null ]
    ] ],
    [ "Lab 0x01", "page_0x01.html", [
      [ "State Diagram", "page_0x01.html#sec_stadia", null ],
      [ "LED Show", "page_0x01.html#sec_LED", null ],
      [ "LED Video", "page_0x01.html#sec_video", null ],
      [ "Source Code Link", "page_0x01.html#sec_code", null ]
    ] ],
    [ "Lab 0x02", "page_0x02.html", [
      [ "Main Script", "page_0x02.html#main", null ],
      [ "User Task", "page_0x02.html#user_task", null ],
      [ "Encoder Task", "page_0x02.html#enc_task", null ],
      [ "Encoder Class", "page_0x02.html#enc_class", null ],
      [ "Shares Class", "page_0x02.html#share_class", null ]
    ] ],
    [ "Lab 0x03 and 0x04", "page_0x034.html", [
      [ "Motor Task", "page_0x034.html#mot_task", null ],
      [ "Motor Class", "page_0x034.html#mot_class", null ],
      [ "Closed Loop Controller", "page_0x034.html#clos_task", null ]
    ] ],
    [ "Lab 0x05", "page_0x05.html", [
      [ "BNO055", "page_0x05.html#sec_IMU", null ]
    ] ],
    [ "Lab 0xFF", "page_0xFF.html", [
      [ "Task Diagram", "page_0xFF.html#sec_task", null ],
      [ "Controller Task", "page_0xFF.html#con_task", null ],
      [ "Data Task", "page_0xFF.html#dat_task", null ],
      [ "IMU Task", "page_0xFF.html#IMU_task", null ],
      [ "Touchpanel Task", "page_0xFF.html#tou_task", null ],
      [ "Video Link", "page_0xFF.html#video", null ]
    ] ],
    [ "Classes", "annotated.html", [
      [ "Class List", "annotated.html", "annotated_dup" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", null ],
        [ "Functions", "functions_func.html", null ],
        [ "Variables", "functions_vars.html", null ]
      ] ]
    ] ],
    [ "Files", "files.html", [
      [ "File List", "files.html", "files_dup" ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
"BNO055_8py.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';